![Build Status](https://gitlab.com/UCL-Pba-ITS/19a-its1-intro/badges/master/pipeline.svg)


# 19A ITS1 intro

weekly plans, resources and other relevant stuff for the intro course in ITS 1. semester

public website for students:

*  [gitlab pages](https://ucl-pba-its.gitlab.io/19a-its1-intro/)
