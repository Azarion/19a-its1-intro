---
Week: 39
Content:  Scripting i bash og powershell
Material: Se links i ugeplanen
Initials: MON
---

# Uge 39

## Mål for ugen

Praktiske og læringsmål for ugen

### Praktiske mål
* Alle har lavet og testet et simpelt bash/shell script
* Alle har lavet og testet et simpelt powershell script

### Lærings mål
* Den studerende kan skrive simple scripts i bash
* Den studerende kan skrive simple scripts i powershell

## Leverancer
Ingen

## Tidsplan

Nedenfor er den foreløbige plan for dagen. Den vil blive opdateret med input fra studerende, m.fl.

### Monday

| Time | Activity |
| :---: | :--- |
| 8:15  | TBA |
| 11:30 | Lunch break |
| 12:15 | TBA |

## Hands-on opgaver

Guided workshop style øvelser. Detaljer kommer på dagen.



## Kommentarer
* [Studerende](https://gitlab.com/ucl-its-19/19a-intro/blob/master/Emner_uge_stud.jpg) står for dele af undervisningen
